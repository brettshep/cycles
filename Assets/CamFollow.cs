﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour {

    
 
    public float smoothTime = 0.1f;
    public float xOffset = 0;
    public Vector2 faSize;
    public Transform target;
    Vector3 currVelocity;
    Vector3 focusPos;
    //Camera cam;
    FocusArea fa;
    Collider2D col;
    Rigidbody targetRb;

    // Update is called once per frame

    private void Start()
    {
  
      //  col = GetComponent<Collider>();
      //  cam = GetComponent<Camera>();
        col = target.GetComponent<Collider2D>();
        fa = new FocusArea(col.bounds, faSize);
      //  targetRb = target.GetComponent<Rigidbody>();
    }
    void LateUpdate () {

        fa.Update(col.bounds);

       focusPos = new Vector3(fa.center.x+xOffset, fa.center.y, transform.position.z);
        //focusPos = new Vector3(target.position.x, target.position.y, transform.position.z);

        transform.position = Vector3.SmoothDamp(transform.position, focusPos, ref currVelocity, smoothTime);

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(fa.center, faSize);
    }

    struct FocusArea
    {
        public Vector2 center;
        public Vector2 velocity;
        float left, right,top,bottom;
        
        public FocusArea(Bounds targetBounds, Vector2 size)
        {
            left = targetBounds.center.x - size.x / 2;
            right = targetBounds.center.x + size.x / 2;
            bottom = targetBounds.min.y;
            top = targetBounds.min.y + size.y;
            center = new Vector2((left + right) / 2, (top + bottom) / 2);
            velocity = Vector2.zero;
        }
        public void Update(Bounds targetBounds)
        {
            //HORIZONTAL
            float shiftX = 0;
            if (targetBounds.min.x < left)
                shiftX = targetBounds.min.x - left;

            else if (targetBounds.max.x > right)
                shiftX = targetBounds.max.x - right;

            left += shiftX;
            right += shiftX;

            //VERTICAL
            float shiftY = 0;
            if (targetBounds.min.y < bottom)
                shiftY = targetBounds.min.y - bottom;

            else if (targetBounds.max.y > top)
                shiftY = targetBounds.max.y - top;

            top += shiftY;
            bottom += shiftY;
            center = new Vector2((left + right) / 2, (top + bottom) / 2);
            velocity = new Vector2(shiftX, shiftY);
        }

    }
}
