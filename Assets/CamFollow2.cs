﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow2 : MonoBehaviour {

    
 
    public float smoothTime = 0.1f;   
    public Transform target;
    Vector3 currVelocity;
    Vector3 focusPos;
   
    void LateUpdate () {

      

        focusPos = new Vector3(target.position.x, target.position.y, transform.position.z);

        transform.position = Vector3.SmoothDamp(transform.position, focusPos, ref currVelocity, smoothTime);

    }

   
}
