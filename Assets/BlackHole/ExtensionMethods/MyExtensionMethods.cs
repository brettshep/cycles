﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MyExtensionMethods
{
    
    
    public static void RotateAroundPointAndLook(this Transform trans, Vector3 point, Vector3 pivot, float angle, float heightOffCircumference, float objectHeight, int leftOrRight)
    {

        trans.position = Quaternion.AngleAxis(angle, Vector3.forward) * ((point - pivot).normalized * (heightOffCircumference + objectHeight) / 2) + pivot;

        
        trans.rotation = Quaternion.LookRotation(
            -Vector3.Cross(Vector3.forward, trans.position - pivot) * leftOrRight,
            trans.position - pivot);
        trans.Rotate(Vector3.forward * 90);


    }

    public static void RotateOnceAroundPoint(this Transform trans, Vector3 point, Vector3 pivot, float angle)
    {

        trans.position = Quaternion.AngleAxis(angle, Vector3.forward) * (point - pivot) + pivot;
        trans.up = trans.position - pivot;
       
    }

    public static void MoveOffScreen(this Transform trans)
    {
        trans.position = Vector3.left * 100;
    }

    public static Color Col(this uint myInt)
    {
        //0x00719F0F
        //0x00719F
        byte R = (byte)((myInt >> 24) & 0xFF);
        byte G = (byte)((myInt >> 16) & 0xFF);
        byte B = (byte)((myInt >> 8) & 0xFF);
        byte A = (byte)((myInt) & 0xFF);
        return new Color32(R, G, B, A);

    }
    public static Color Col(this int myInt)
    {
        //0x00719F0F
        //0x00719F
        byte R = (byte)((myInt >> 24) & 0xFF);
        byte G = (byte)((myInt >> 16) & 0xFF);
        byte B = (byte)((myInt >> 8) & 0xFF);
        byte A = (byte)((myInt) & 0xFF);
        return new Color32(R, G, B, A);

    }
}
