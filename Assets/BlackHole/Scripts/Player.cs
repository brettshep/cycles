﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public SpawnController sc;
    public float moveSpeed = 10f;
    public float radius;
    public bool invincible = false;
    public GameObject particles;
    public bool arrows;


    private void Start()
    {
        //scoreText.text = "0";
    }

    void Update () {

        radius = Vector3.Distance(transform.position, Vector3.zero);

        if (arrows)
        {
            if (TouchManager.current.LeftTouch)
            {
                transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, moveSpeed * Time.deltaTime);
            }
            else if (TouchManager.current.RightTouch)
            {
                Vector3 dir = (Vector3.zero + transform.position) * 100;
                transform.position = Vector3.MoveTowards(transform.position, dir, moveSpeed * Time.deltaTime);
            }
        }
        else
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 dir = (Vector3.zero + transform.position) * 100;
                transform.position = Vector3.MoveTowards(transform.position, dir, moveSpeed * Time.deltaTime);
            }
            else 
            {
                transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, moveSpeed * Time.deltaTime);
            }
            


        }
        


	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!invincible)
        {
            if (collision.CompareTag("Collectable"))
            {
                collision.GetComponent<ICollectable>().Collect(this);
                sc.coinsCollected++;
            }
                

            else if (collision.CompareTag("Enemy"))
                Die();

            else if (collision.CompareTag("ColBox"))
                Die();
            else if (collision.CompareTag("BlackHole"))
                Die();
        }
       

    }

    void Die()
    {
        AudioManager.Instance.Play("PlayerDie");
        particles.transform.position = transform.position;
        particles.SetActive(true);
        Destroy(gameObject);
    }
}
