﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSpin : MonoBehaviour {

    public float spinSpeedMultiplier = 1f;
    public Player ps;
    public static float rotations;
    float spinSpeed;
    float rotAdder = 0;

    private void Start()
    {
        rotations = 0;
    }

    void Update () {

        
        if (ps.radius > 0)
            spinSpeed = spinSpeedMultiplier / (Mathf.Sqrt(ps.radius));

        transform.Rotate(Vector3.forward * -spinSpeed * Time.deltaTime);

        //add rotations
        rotAdder += spinSpeed * Time.deltaTime;
        rotations = rotAdder / 360f;

    }
}
