﻿using UnityEngine;
 
public class SizePulse : Effect
{

    private float maxSize = 0.4f;
    private float minSize = 0.2f;
    private float speed = 3f;
    private float range;

    public SizePulse(float max, float min, float pulseSpeed)
    {
        maxSize = max;
        minSize = min;
        speed = pulseSpeed;
    }

    public void Act(Enemy enemy)
    {

         range = maxSize - minSize;
         enemy.transform.localScale = Vector3.one * ((Mathf.Sin(Time.time * speed) + 1.0f) / 2.0f * range + minSize);

    }


}