﻿using UnityEngine;

public class RotateAround : Effect
{
    private float speed = 3f;
    private float sinSpeed = 2f;
    public RotateAround(float moveSpeed, float sinspeed)
    {
        speed = moveSpeed;
        sinSpeed = sinspeed;
    }

    public void Act(Enemy enemy)
    {

        enemy.transform.RotateAround(Vector3.zero, Vector3.forward, speed * sinSpeed * 
            Time.deltaTime * Mathf.Sin(sinSpeed * (Time.time - enemy.timeOfSpawn)));

    }




}