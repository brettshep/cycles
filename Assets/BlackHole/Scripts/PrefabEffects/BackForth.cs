﻿using UnityEngine;


public class BackForth : Effect
{
    private float distanceFromCenter = 1f;
    private float moveSpeed = 1;
    private float moveDistance = 2;

    public BackForth(float speed, float moveDist)
    {
        moveSpeed = speed;
        moveDistance = moveDist;
    }

    public void Act(Enemy enemy)
    {
        enemy.transform.Translate(enemy.directionAwayFromCenter * moveDistance * moveSpeed * Time.deltaTime
            * Mathf.Sin(moveSpeed*(Time.time - enemy.timeOfSpawn)), Space.World );
    }


}