﻿using UnityEngine;

public class MoveToCenter : Effect
{
    private float speed = 3f;

    public MoveToCenter(float moveSpeed)
    {
        speed = moveSpeed;
    }

    public void Act(Enemy enemy)
    {

        enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, Vector3.zero, speed * Time.deltaTime);

    }




}
