﻿using UnityEngine;

public class CircleMotion : Effect
{
    private float moveSpeed = 1f;
    private float rotateSpeed = 10f;

    public CircleMotion(float movSpeed, float rotSpeed)
    {
        moveSpeed = movSpeed;
        rotateSpeed = rotSpeed;
    }

    public  void Act(Enemy enemy)
    {
        enemy.transform.Translate(Time.deltaTime * moveSpeed, 0, 0 ); // move forward
        enemy.transform.Rotate(0, 0, Time.deltaTime * rotateSpeed); // turn a little
    }

    
}