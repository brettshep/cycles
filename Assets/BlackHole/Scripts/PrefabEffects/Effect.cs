﻿using UnityEngine;
 
public interface Effect
{
    void Act(Enemy enemy);
}