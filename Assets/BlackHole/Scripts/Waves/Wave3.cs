﻿using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu(menuName = "Wave/Wave3", fileName = "Wave3")]
public class Wave3 : Wave
{

    public override void SetUpSpawnerForWave(SpawnController spawner)
    {

        spawner.waveLines.Clear();

        //line 1
        spawner.waveLines.Add(
            new WaveLine(
                0.2f,                           //enemy Size
                new float[] { },                //enemy spawn positions
                new float[] { Random.value },   //coin spawn positions
                new List<Effect>()              //effect list
                
            )
        );

        //line 2
        spawner.waveLines.Add(
            new WaveLine(
                0.2f,                           //enemy Size
                new float[] { 0.75f, 1f },   //enemy spawn positions
                new float[] { },           //coin spawn positions
                new List<Effect>()              //effect list
                {
                    new MoveToCenter(0.3f)
                }
             )
        );

        //line 3
        spawner.waveLines.Add(
            new WaveLine(
                0.2f,                           //enemy Size
                new float[] { },                //enemy spawn positions
                new float[] { Random.value },   //coin spawn positions
                new List<Effect>()              //effect list

            )
        );

        //line 4
        spawner.waveLines.Add(
            new WaveLine(
                0.2f,                           //enemy Size
                new float[] { 0.5f, 1f },   //enemy spawn positions
                new float[] { },           //coin spawn positions
                new List<Effect>()              //effect list
                {
                    new MoveToCenter(0.3f)
                }
             )
        );
        
        
        spawner.spawnInterval = 0.05f;
        spawner.spawnRotLen = 1f;
        spawner.loopSpawn = true;
        spawner.dieIntervalMult = 2; //only matters if loop
        spawner.waveCoinCount = 5;

    }
}