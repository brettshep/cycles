﻿using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu(menuName = "Wave/Wave2", fileName = "Wave2")]
public class Wave2 : Wave
{

    public override void SetUpSpawnerForWave(SpawnController spawner)
    {

        spawner.waveLines.Clear();

        //line 1
        spawner.waveLines.Add(
            new WaveLine(
                1f,                           //enemy Size
                new float[] { 0.5f },   //enemy spawn positions
                new float[] { 0f, 1f },   //coin spawn positions
                new List<Effect>()              //effect list
                {
                    new CircleMotion(2f,200f)
                }
            )
        );


        spawner.spawnInterval = 0.25f;
        spawner.loopSpawn = false;
        spawner.dieIntervalMult = 1; //only matters if loop
        spawner.waveCoinCount = 3;

    }
}