﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Wave/Wave4", fileName = "Wave4")]
public class Wave4 : Wave
{

    public override void SetUpSpawnerForWave(SpawnController spawner)
    {

        spawner.waveLines.Clear();

        //line 1
        spawner.waveLines.Add(
            new WaveLine(
                0.5f,                             //enemy Size
                new float[] {0f},   //enemy spawn positions
                new float[] { 0.25f, 0.75f },   //coin spawn positions
                new List<Effect>()              //effect list
                {
                    new BackForth(2f, 1.5f)
                }
            )
        );

        

        //wave settings
        spawner.loopSpawn = false;
        spawner.dieIntervalMult = 2; //only matters if loop
        spawner.spawnInterval = 0.2f;
        spawner.waveCoinCount = 4;

    }
}