﻿using UnityEngine;
using System.Collections.Generic;

public abstract class Wave : ScriptableObject
{
    public abstract void SetUpSpawnerForWave(SpawnController spawner);

}

public struct WaveLine
{
    public float enemySize;
    public float[] enemySpawnPositions;
    public float[] coinSpawnPositions;
    public List<Effect> lineEffects;

    public WaveLine(float enemSize, float[] enemySpawnPos, float[] coinSpawnPos, List<Effect> lineEffs )
    {
        enemySize = enemSize;
        enemySpawnPositions = enemySpawnPos;
        coinSpawnPositions = coinSpawnPos;
        lineEffects = lineEffs;
    }
}
    
