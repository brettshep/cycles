﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Wave/Wave1", fileName ="Wave1")]
public class Wave1 : Wave
{

    public override void SetUpSpawnerForWave(SpawnController spawner)
    {

        spawner.waveLines.Clear();

        //line 1
        spawner.waveLines.Add(
            new WaveLine(
                0.2f,                           //enemy Size
                new float[] { 0f, 0.5f, 1f },   //enemy spawn positions
                new float[] { 0.25f, 0.75f },   //coin spawn positions
                new List<Effect>()              //effect list
                {
                    new SizePulse(0.4f, 0.15f, 2f)
                }
            )
        );
      
        //line 2
        spawner.waveLines.Add(
            new WaveLine(
                0.2f,                           //enemy Size
                new float[] { 0.25f, 0.75f },   //enemy spawn positions
                new float[] { 0.5f },           //coin spawn positions
                new List<Effect>()              //effect list
                {
                    new SizePulse(0.4f, 0.15f, 2f)
                }
             )
        );

        //wave settings
        spawner.loopSpawn = false;
        spawner.dieIntervalMult = 2; //only matters if loop
        spawner.spawnInterval = 0.1f;
        spawner.waveCoinCount = 4;

    }
}