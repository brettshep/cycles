﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Wave/Wave6", fileName = "Wave6")]
public class Wave6 : Wave
{

    public override void SetUpSpawnerForWave(SpawnController spawner)
    {

        spawner.waveLines.Clear();

        //line 1
        spawner.waveLines.Add(
            new WaveLine(
                0.75f,                             //enemy Size
                new float[] { 0.5f },   //enemy spawn positions
                new float[] { 0, 0.25f, 0.75f, 1f },   //coin spawn positions
                new List<Effect>()              //effect list
                {
                    new RotateAround(-15f, 3f),
                    //new BackForth(2f, .5f)
                }
            )
        );
        //line 1
        spawner.waveLines.Add(
            new WaveLine(
                0.7f,                             //enemy Size
                new float[] { 0.25f, 0.75f },   //enemy spawn positions
                new float[] { 0, 0.5f, 1f },   //coin spawn positions
                new List<Effect>()              //effect list
                {
                    new SizePulse(1f, 0.5f, 2f)
                    //new BackForth(2f, .5f)
                }
            )
        );



        //wave settings
        spawner.loopSpawn = false;
        spawner.dieIntervalMult = 2; //only matters if loop
        spawner.spawnInterval = 0.1666f;
        spawner.waveCoinCount = 4;

    }
}