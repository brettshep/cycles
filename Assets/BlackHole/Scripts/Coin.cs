﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour , ICollectable {


    public void Collect(Player ps)
    {
        AudioManager.Instance.Play("Collect");
        gameObject.SetActive(false);
    }


}
