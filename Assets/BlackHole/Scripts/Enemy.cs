﻿using UnityEngine;
using System.Collections.Generic;

public class Enemy : MonoBehaviour
{

    #region Public Fields
    [Header("Vals")]
    public float fadeTime = 2f;

    [Header("Refs")]
    SpawnController sc;
    
    //Hide in inspector
    [HideInInspector] public Vector3 directionAwayFromCenter; 
    [HideInInspector] public float rotsAtSpawn;
    [HideInInspector] public float timeOfSpawn;
    #endregion

    #region Private Fields
    private bool loop = false;
    private float loopDieIterOffset;
    private int waveOfSpawn;
    private CircleCollider2D col;
    private List<Effect> currEffects = new List<Effect>();
    #endregion

    #region Unity Methods

    private void Awake()
    {
        col = GetComponent<CircleCollider2D>();
    }

    void Update()
    {
        directionAwayFromCenter = (transform.position - Vector3.zero).normalized;
        if (SpawnController.SC.currWaveNum != waveOfSpawn)
            Die();
        else if (loop && SpawnController.SC.currWaveRots - rotsAtSpawn >= loopDieIterOffset)
            Die();
        else
        {
            for (int i = 0; i < currEffects.Count; i++)
            {
                currEffects[i].Act(this);
            }         
        }       
    }

    #endregion

    #region Private Methods
    public void Spawn(List<Effect> effects, float currWaveRots, int currWaveNum, bool loopWave, float iterOffset)
    {
        loop = loopWave;
        currEffects = effects;
        rotsAtSpawn = currWaveRots;
        waveOfSpawn = currWaveNum;
        col.enabled = true;
        directionAwayFromCenter = (transform.position - Vector3.zero).normalized;
        loopDieIterOffset = iterOffset;
        timeOfSpawn = Time.time;
        
    }


    void Die()
    {
        col.enabled = false;
        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, fadeTime * Time.deltaTime);
        if (transform.localScale.x <= 0.05)
        {           
            gameObject.SetActive(false);
        }
    }

    #endregion
}