﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour {
    public static TouchManager current;
    public bool LeftTouch = false;
    public bool RightTouch = false;
    public bool TouchDown = false;
    public bool TouchHold = false;

    // Update is called once per frame
    private void Awake()
    {
        current = this;
    }

    void Update ()
    {
        TouchRegionCheck();
    }

    void TouchRegionCheck()
    {
        if(Input.GetMouseButton(0))
        {
            //TouchDown = true;
            
            if (Input.mousePosition.x < Screen.width / 2)
                 LeftTouch = true;

            else if (Input.mousePosition.x > Screen.width / 2)
                RightTouch = true;
             
        }
        else if(Input.GetAxisRaw("Horizontal") < 0)
            LeftTouch = true;
        else if (Input.GetAxisRaw("Horizontal") > 0)
            RightTouch = true;
        else
        {
           // TouchDown = false;
            LeftTouch = false;
            RightTouch = false;
        }
    }

    

}
