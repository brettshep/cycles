﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SpawnController : MonoBehaviour
{

    #region Public Fields
    
    //Static Instance
    [HideInInspector]
    public static SpawnController SC;

    //----SPAWN CONTROL VALUES----

    [Header("Coin Options")]
    [Range(0, 1)] public float coinChancePerLine = 0.5f;
    [Range(0, 1)] public float coinMultipleDifficulty = 0.1f;
    public float coinSize = 0.2f;

    [Header("Spawn Offset Options")]
    public float holeSpawnOffset = 0.5f;
    public float outerSpawnOffset = 0.5f;

    [Header("Refs")] 
    public Transform blackHole;
    public SpriteRenderer outerBoundsSprite;
    public NewWaveText waveText;
    public Text scoreText;
    public Transform playerTrans;
        
    [Header("Waves")]
    public Wave[] wavesArr;

    //-----HIDE IN INSPECTOR------

    //Set In Wave
    [HideInInspector] public float spawnRotLen = 1;
    [HideInInspector] public bool loopSpawn;
    [HideInInspector] public float spawnInterval;  
    [HideInInspector] public List<WaveLine> waveLines;
    //Current Wave Info
    [HideInInspector] public float currWaveRots;
    [HideInInspector] public int currWaveNum;
    [HideInInspector] public int waveCoinCount;
    [HideInInspector] public int coinsCollected;
    [HideInInspector] public int dieIntervalMult = 0;


    #endregion

    #region Private Fields

    //wave info
    Wave currWave;
    float waveStartRots;
    int maxRowsThisWave = 0;
    
    //spawn info
    int spawnIterations = 0;
    int spawnedCoinNum;
    int currWaveLineIndex = 0;

    List<int> coinSpawnIndices;
    List<int> possiblCoinIndices;

    Vector3 p1;
    Vector3 p2;
    float p1Offset;
    float p2Offset;
   
    #endregion

    #region Unity Methods

    void Start()
    {
        //Set Static Instance
        SC = this;
        
        //Initialize Variable
        waveStartRots = 0;
        currWaveRots = 0;
        spawnRotLen = 1;
        coinSpawnIndices = new List<int>();
        possiblCoinIndices = new List<int>();
        waveLines = new List<WaveLine>();

        //Set Spawn Location Points
        p1Offset = (blackHole.localScale.y / 2 + holeSpawnOffset);
        p2Offset = (outerBoundsSprite.bounds.size.y / 2 - outerSpawnOffset);

        //Load Intial Wave Info
        LoadNewWave();

    }

    private void Update()
    {
        if (coinsCollected >= waveCoinCount)
            LoadNewWave();
        //set wave rots
        currWaveRots = RotateSpin.rotations - waveStartRots;
        SpawnWave();
    }

    #endregion

    #region Private Methods

    void SpawnWave()
    {
        
        //check if we have spawned all lines this waves
        if (spawnIterations < maxRowsThisWave || loopSpawn)
        {
            //see if the current waveRots are greater than the line to be spawned
            if (currWaveRots >= spawnIterations * spawnInterval)
            {
                    
                //---------SPAWN LINE OF OBJECTS---------

                //------Spawn Enemy------

                //loop through enemy list
                for (int i = 0; i < waveLines[currWaveLineIndex].enemySpawnPositions.Length; i++)
                {
                    //spawn with correct transformations
                    GameObject enemy = PoolManager.self.GetObject((int)PoolManager.ObjName.enemy);
                    enemy.transform.localScale = Vector3.one * waveLines[currWaveLineIndex].enemySize;
                    enemy.transform.position = Quaternion.AngleAxis(-spawnInterval * spawnIterations * 360, Vector3.forward) *
                        Vector3.Lerp(p1, p2, waveLines[currWaveLineIndex].enemySpawnPositions[i]);

                    //Do stuff with enemy script
                    Enemy enemyScript = enemy.GetComponent<Enemy>();
                    enemyScript.Spawn(waveLines[currWaveLineIndex].lineEffects, currWaveRots, currWaveNum, loopSpawn, spawnRotLen - (spawnInterval * dieIntervalMult));
                                         

                    //Activate if not already
                    enemy.SetActive(true);
                }
                    
                //------Spawn Coins------

                //if coin is in set, then spawn
                if (coinSpawnIndices.Contains(spawnIterations) && waveLines[currWaveLineIndex].coinSpawnPositions.Length > 0)
                {   
                    //increment num of coins spawned
                    spawnedCoinNum++;

                    //spawn with correct transformations
                    GameObject coin = PoolManager.self.GetObject((int)PoolManager.ObjName.coin);
                    coin.transform.localScale = Vector3.one * coinSize;
                    var spawnIndex = Random.Range(0, waveLines[currWaveLineIndex].coinSpawnPositions.Length);
                    coin.transform.position = Quaternion.AngleAxis(-spawnInterval * spawnIterations * 360, Vector3.forward) *
                        Vector3.Lerp(p1, p2, waveLines[currWaveLineIndex].coinSpawnPositions[spawnIndex]);

                    //Activate if not already
                    coin.SetActive(true);

                }

                //loop waveLine
                currWaveLineIndex++;
                if (currWaveLineIndex >= waveLines.Count)
                    currWaveLineIndex = 0;

                //spawn line iterations
                spawnIterations++;

            }
            
        }

    }


    void LoadNewWave()
    {
        //wave transition sound
        AudioManager.Instance.Play("WaveTrans");
        // start lerping BG
        BackgroundManager.BM.NewWaveColors();

        //load temp new wave text 
        waveText.NewWaveTextAppear();      
        
        //set current wave
        currWave = wavesArr[currWaveNum];
        //set wave text
        scoreText.text = currWaveNum.ToString();
        
        //loop wave number
        currWaveNum++;
        if (currWaveNum >= wavesArr.Length)
            currWaveNum = 0;

        //load wave
        currWave.SetUpSpawnerForWave(this);

        //set vars after wave load    
        waveStartRots = RotateSpin.rotations;
        spawnIterations = 0;
        maxRowsThisWave = Mathf.RoundToInt(spawnRotLen / spawnInterval);

        //---choose random lines coins can be on ---
        coinSpawnIndices.Clear();
        possiblCoinIndices.Clear();

        //fill possible options
        int lineIndex = 0;
        for (int i = 0; i < maxRowsThisWave; i++)
        {
            if(waveLines[lineIndex].coinSpawnPositions.Length > 0)
                possiblCoinIndices.Add(i);
            lineIndex++;
            if (lineIndex >= waveLines.Count)
                lineIndex = 0;
        }

        //get random indices
        if (waveCoinCount > possiblCoinIndices.Count)
            Debug.LogError("Not enough spaces for Coins! Decrease wave amount or spawn interaval.");

        for (int i = 0; i < waveCoinCount; i++)
        {
            var rand = Random.Range(0, possiblCoinIndices.Count);   
            coinSpawnIndices.Add(possiblCoinIndices[rand]);
            possiblCoinIndices.RemoveAt(rand);

        }

        
        //set wave spawn originPoints
        p1 = -playerTrans.position.normalized * p1Offset;
        p2 = -playerTrans.position.normalized * p2Offset;

        coinsCollected = 0;
        currWaveLineIndex = 0;


}

     
   
    #endregion
}


