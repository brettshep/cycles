﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    [Header("Values")]
    public float spawnRate = 0.5f;
    public float offsetFromHole = 1;
    [Range(0, 100)]
    public int coinChance;

    [Header("SpawnType")]
    public bool randomSpawn = true;
    public int numPerLine = 5;
    public bool orderedSpawn;
    public int orderSpawnSpaces;

    [Header("Refs")]
    public Transform enemyPrefab;
    public Transform coinPrefab;
    public SpriteRenderer circleSprite;
    public Transform blackHole;

    Vector3[] positionsArray;
    float maxHeight;
    float enemyHeight;
    int maxNumOfEnemy;
    float holeRadius;
    float holeOffsetDist;
    List<int> randomIndexs = new List<int>();
    int coinIndex;

    void Start () {
        holeRadius = blackHole.localScale.y / 2;
        holeOffsetDist = holeRadius + offsetFromHole;
        maxHeight = circleSprite.bounds.size.y/2 - (holeOffsetDist);
        enemyHeight = enemyPrefab.transform.localScale.y;
        maxNumOfEnemy = Mathf.FloorToInt(maxHeight / enemyHeight);
        positionsArray = new Vector3[maxNumOfEnemy];
       
        StartCoroutine(Spawn());

	}

    IEnumerator Spawn()
    {
        while (true)
        {
            FillArray();
            
            
            //random spawn
            if (randomSpawn)
            {
                randomIndexs.Clear();
                for (int i = 0; i < numPerLine; i++)
                {
                    int randNum = Random.Range(0, positionsArray.Length);
                    while (randomIndexs.Contains(randNum))
                    {
                        randNum = Random.Range(0, positionsArray.Length);
                    }
                    randomIndexs.Add(randNum);
                }
            }
            //ordered spawn
            else if (orderedSpawn)
            {
                randomIndexs.Clear();
                for (int i = 0; i < positionsArray.Length; i++)
                {               
                    if (i%orderSpawnSpaces == 0)
                        randomIndexs.Add(i);
                }
            }

            //coin chance
            if (Random.value * 100 <= coinChance)
                coinIndex = Random.Range(0, randomIndexs.Count);
            else
                coinIndex = 100; //large number not possible for indexs


            for (int i = 0; i < randomIndexs.Count; i++)
            {
                GameObject prefab;
                if(i != coinIndex)
                {
                    prefab = PoolManager.self.GetObject((int)PoolManager.ObjName.enemy);
                    prefab.transform.localScale = enemyPrefab.localScale;
                }

                else
                {
                    prefab = PoolManager.self.GetObject((int)PoolManager.ObjName.coin);
                    prefab.transform.localScale = coinPrefab.localScale;
                }
                    
             
                prefab.transform.position = positionsArray[randomIndexs[i]];
                prefab.transform.RotateOnceAroundPoint(prefab.transform.position, Vector3.zero, 200);
                prefab.SetActive(true);
            }
           
            
            
            yield return new WaitForSeconds(spawnRate);
        }
    }

    void FillArray()
    {
        for (int i = 0; i < positionsArray.Length; i++)
        {
            positionsArray[i] = (transform.position - blackHole.position).normalized * (i * enemyHeight + holeOffsetDist);
        }
    }
}
