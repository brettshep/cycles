﻿using UnityEngine;
using System.Collections.Generic;

public class BackgroundManager : MonoBehaviour
{

    #region Public Fields
    public static BackgroundManager BM;
    public SFRenderer sf;
    public float lerpSpeed = 1f;
    public Renderer bgMesh; 
    #endregion

    #region Private Fields
    private List<WaveColor> waveColorList;
    private int currWaveColIndex;
    private float currentT;
    private Color prevFog;
    private Color prevScatter;
    private Color prevAmbient;
    private Color prevCol1;
    private Color prevCol2;
    private bool keepLerping;
    private WaveColor currWaveCol;
    //shader vars
    private Material bgMat;
    private int col1;
    private int col2;
    private bool firstTime;
    #endregion

    #region Unity Methods

    void Start()
    {
       BM = this;
       bgMat = bgMesh.material;
       col1 = Shader.PropertyToID("_ColorA");
       col2 = Shader.PropertyToID("_ColorB");
       firstTime = true;
       currWaveColIndex = 0;
       ListSetup();      
    }

    private void Update()
    {
        if (keepLerping)
        {
            LerpColors();
        }
            
        
    }

    #endregion

    #region Private Methods
    public void NewWaveColors()
    {
        //set base cols to be interpolated from
        prevFog = sf.fogColor;
        prevAmbient = sf.ambientLight;
        prevScatter = sf.scatterColor;
        prevCol1 = waveColorList[currWaveColIndex].col1;
        prevCol2 = waveColorList[currWaveColIndex].col2;

        //add and loop index of list
        currWaveColIndex++;
        if (currWaveColIndex >= waveColorList.Count)
            currWaveColIndex = 0;
        
        //first round reset
        if (firstTime)
        {
            currWaveColIndex = 0;
            firstTime = false;
        }

        //set current wave color
            currWaveCol = waveColorList[currWaveColIndex];

        currentT = 0;
        keepLerping = true;

    }

    private void LerpColors()
    {

        //increase T
        currentT += Time.deltaTime * lerpSpeed;

         //set shadow colors
        sf.fogColor = Color.Lerp(prevFog, currWaveCol.fogCol, currentT);
        sf.ambientLight = Color.Lerp(prevAmbient, currWaveCol.ambientCol, currentT);
        sf.scatterColor = Color.Lerp(prevScatter, currWaveCol.scatterCol, currentT);

        //set bg colors
        bgMat.SetColor(col1, Color.Lerp(prevCol1, currWaveCol.col1, currentT));
        bgMat.SetColor(col2, Color.Lerp(prevCol2, currWaveCol.col2, currentT));
        
        //test if done interpolating
        if (currentT > 1)
            keepLerping = false;
    }

    private void ListSetup()
    {
        waveColorList = new List<WaveColor>()
        {                              
            new WaveColor(0x0002FFFF.Col(), 0x00719F0F.Col(), Color.black, 0x00EAFFFF.Col() , 0x0056FFFF.Col()),
            new WaveColor(0x2A0C1EFF.Col(), 0x842AD40F.Col(), Color.black, 0xE46BFFFF.Col() , 0xA500DFFF.Col()),
            new WaveColor(0x110005FF.Col(), 0x984B690F.Col(), 0x00000079.Col(),  0xD8BF03FF.Col() , 0xFF1F65FF.Col())
        };
    }
    #endregion
}

//--------WAVE COLOR STRUCT---------

public struct WaveColor
{
    public Color ambientCol;
    public Color fogCol;
    public Color scatterCol;
    public Color col1;
    public Color col2;

    public WaveColor(Color ambient, Color fog, Color scatter, Color color1, Color color2)
    {
        ambientCol = ambient;
        fogCol = fog;
        scatterCol = scatter;
        col1 = color1;
        col2 = color2;
    }
}