﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour {
    public static PoolManager self;
    public enum ObjName {enemy=0, coin=1}; //must be in same order as Object Array!!! CAN EVENTUALLY GET RID OF
    public GameObject[] gameObjectArray;
    public int[] spawnNumArray;

    private int[] iteratorArr;
    private List<GameObject>[] objectPool;
    private GameObject objToReturn;

    private void Awake()
    {
        self = this;
        PopulateArray();
    }

  
    public GameObject GetObject(int index)
    {
        iteratorArr[index] += 1;
        if (iteratorArr[index] >= spawnNumArray[index])
            iteratorArr[index] = 0;

        objToReturn = objectPool[index][iteratorArr[index]];
       // objToReturn.SetActive(false);

        return objToReturn;
    }

	void PopulateArray()
    {
        int xLength = gameObjectArray.Length;
        iteratorArr = new int[xLength];
        objectPool = new List<GameObject>[xLength];
        GameObject temp;

        for (int x = 0; x < xLength; x++)
        {
            objectPool[x] = new List<GameObject>();
            for (int y = 0; y < spawnNumArray[x]; y++)
            {
                temp = Instantiate(gameObjectArray[x]);
                temp.transform.MoveOffScreen();
                temp.SetActive(false);
                objectPool[x].Add(temp);
                
            }
        }


    }
}
