﻿using UnityEngine;
using UnityEngine.UI;
public class NewWaveText : MonoBehaviour
{

    #region Public Fields
    public float fadeTime = 0.5f;
    #endregion

    #region Private Fields
    float currTime;
    #endregion

    private void Update()
    {
        currTime -= Time.deltaTime;
        if (currTime <= 0)
            gameObject.SetActive(false);
    }


    #region Private Methods
    public void NewWaveTextAppear()
    {
        gameObject.SetActive(true);
        currTime = fadeTime;

    }
    #endregion
}